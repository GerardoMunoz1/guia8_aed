# Guía 8 - Métodos de Ordenamiento Interno

## Ingeniería Civil en Bioinformática - Universidad de Talca

### Pre-requisitos

Para ejecutar correctamente el programa, se debe tener instalado un compilador de C++.

### Instalación

Para instalar el programa, se debe descargar el presente repositorio en una carpeta existente o no. 

```
$ cd carpeta_existente
```
Dentro de la carpeta, ejecutamos el archivo MakeFile:

```
$ make
```

Para correr correctamente el programa, escribimos en la terminal:

```
$ ./programa [parametro n°1] [parametro n°2]
```

En donde "parametro n°1" y "parametro n°2" corresponden al modo de uso del programa, siendo el primer parametro el número a calcular y el segundo paramtro la opción de mostrar todos los resultados o no (s/n).

Ejemplo:

```
$ ./programa 10 s
```
