#include <iostream>
#include <time.h>
#include "Quicksort.h"

using namespace std;

// Constructor default
Quicksort::Quicksort(){}

void Quicksort::reduce(int ini, int fin, int &pos, int *array){

    int aux;
    int izq = ini;
    int der = fin;
    bool sorting = 1;
    pos = ini;

    while(sorting){
        while(array[pos] <= array[der] && pos != der){
            der--;
        }
        if(pos == der){
            sorting = 0;
        }
        else{
            aux = array[pos];
            array[pos] = array[der];
            array[der] = aux;
            pos = der;

            while(array[pos] >= array[izq] && pos != izq){
                izq++;
                aux = array[pos];
                array[pos] = array[izq];
                array[izq] = aux;
                pos = izq;
            }
    }
}
}

// Método quicksort, entrega como resultado el tiempo final de ejecución
double Quicksort::quicksort(int *array, int n){

    clock_t inicio = clock();
    int pilamenor[n];
    int pilamayor[n];
    int ini;
    int fin;
    int pos;

    int tope = 0;
    pilamenor[tope] = 0;
    pilamayor[tope] = n-1;

    while(tope >=0 ){
        ini = pilamenor[tope];
        fin = pilamayor[tope];
        tope--;
        this->reduce(ini, fin, pos, array);

        if(ini < (pos-1)){
            tope++;
            pilamenor[tope] = ini;
            pilamayor[tope] = pos-1;
        }
        if(fin > (pos+1)){
            tope++;
            pilamenor[tope] = pos+1;
            pilamayor[tope] = fin;
        }
    }
    return (double)(clock()-inicio)/CLOCKS_PER_SEC * 1000;
}
