#include <iostream>

using namespace std;

#ifndef QUICKSORT_H
#define QUICKSORT_H

// Definición de la clase Quicksort y sus atributos respectivos.
class Quicksort{

    private:
        void reduce(int ini, int fin, int &pos, int *array);

    public:
        Quicksort();
        double quicksort(int *array, int n);
};
#endif
