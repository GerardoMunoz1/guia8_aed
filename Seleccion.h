#include <iostream>

using namespace std;

#ifndef SELECCION_H
#define SELECCION_H

// Definición de la clase Seleccion y sus atributos respectivos.
class Seleccion{

    public:
        Seleccion();
        double ordenar_seleccion(int *array, int n);
};
#endif
