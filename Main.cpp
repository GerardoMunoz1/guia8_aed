#include <iostream>
#include <time.h>
#include <stdlib.h>
#include "Seleccion.h"
#include "Quicksort.h"

using namespace std;

// Función para imprimir tiempos finales de ejecución
void imprimir_tiempos(double *tiempo, string *metodo){

    cout << "\n\t [ METODOS DE ORDENAMIENTO ] " << endl;
    cout << "==================================================" << endl;
    cout << "  Método             |  Tiempo Utilizado (ms)" << endl;
    cout << "==================================================" << endl;

    for(int i=0; i<2; i++){
        cout << metodo[i] << string(21-metodo[i].length(), ' ') << "|" << "  " << tiempo[i] << " [ms]"<< endl;
    }
    cout << "__________________________________________________" << endl;
}

// Función para generar y ordenar los números del vector creado
void generar_y_ordenar(int n, string arg){

    Seleccion seleccion;
    Quicksort quicksort;
    int random;
    int vector[3][n];
    double tiempo[2];
    srand(time(0));

    for(int i=0; i<n; i++){
        random = rand()%n + 1;

        for(int j=0; j<3; j++){
            vector[j][i] = random;
        }
    }

    tiempo[0] = seleccion.ordenar_seleccion(vector[0], n);
    tiempo[1] = quicksort.quicksort(vector[1], n);

    string metodo[2] = {"Seleccion", "Quicksort"};

    // Llamado para imprimir tiempos de ejecución por pantalla
    imprimir_tiempos(tiempo, metodo);

    if(arg == "s"){
        cout << "\n> Mostrando contenido del vector principal...";
        cout << "\n==================================================" << endl;
        cout << "\t[ VECTOR PRINCIPAL ]" << endl << "\n| ";

        for(int i=0; i<n; i++){
            cout << vector[2][i] << " > ";
        }
        cout << "|" << endl;
        cout << "==================================================" << endl;

        for(int i=0; i<2; i++){
            cout << metodo[i] << string(21-metodo[i].length(), ' ') << "|";

            for(int j=0; j<n; j++){
                cout << vector[i][j] << "|";
            }
            cout << endl;
        }

    }
}

// Función principal (main)
int main(int argc, char **argv){

    int n;
    string arg;

    if (argc != 3){
        cout << "\nOops! Debe ingresar 2 parámetros.";
        cout << "\nEJEMPLO: ./programa [numero] [s/n]" << endl;
    }
    else{
        try{
            n = stoi(argv[1]);
            arg = argv[2];

            if(n > 1000000 || n < 1){
                cout << "El tamaño ingresado para el vector no es válido: ";
                cout << "debe ser un número positivo menor a 1.000.000" << endl;
                exit(1);
            }

            else if (arg != "n" && arg != "s"){
                cout << "Whoops! Parámetro no válido, utiliza 's' o 'n' como parámetro.";
                cout << "\nEJEMPLO: ./programa [numero] [s/n]" << endl;
                exit(1);
            }
        }
        catch (const std::invalid_argument& e){
            cout << "Error. Debe ingresar un número en el primer parámetro.";
            cout << "\nEJEMPLO: ./programa [numero] [s/n]" << endl;
            exit(1);
        }
        // Llamado a la funcion para generar y ordenar el vector
        generar_y_ordenar(n, arg);
    }

    return 0;
}
